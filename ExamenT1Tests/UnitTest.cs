﻿using ExamenT1;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenT1Tests
{

    [TestFixture]
    class UnitTest
    {

        Game game;

        [SetUp]
        public void SetUpGame()
        {
            game = new Game();
        }
        

        [Test]
        public void TestCaso01()// 0 Pinos
        {

            //var games = new Game();

            game.NumeroPino(0);
            
            var resEsperado = 0;

            var resFinal = game.Puntuacion();

            Assert.AreEqual(resFinal, resEsperado);

        }


        [Test]
        public void TestCaso02()//1 pino
        {

            game.NumeroPino(1);

            var resEsperado = 1;

            var resFinal = game.Puntuacion();

            Assert.AreEqual(resFinal, resEsperado);
        }


        [Test]
        public void TestCaso03()//10 pino
        {

            game.NumeroPino(10);

            var resEsperado = 10;

            var resFinal = game.Puntuacion();

            Assert.AreEqual(resFinal, resEsperado);
        }


        [Test]
        public void TestCaso04()//10 lansamientos
        {

            game.NumeroPino(10);
            game.NumeroPino(9);     game.NumeroPino(1);
            game.NumeroPino(5);     game.NumeroPino(5);
            game.NumeroPino(7);     game.NumeroPino(2);
            game.NumeroPino(10);
            game.NumeroPino(10);
            game.NumeroPino(10);
            game.NumeroPino(9);     game.NumeroPino(0);
            game.NumeroPino(8);     game.NumeroPino(2);
            game.NumeroPino(9);     game.NumeroPino(1);     game.NumeroPino(10);


            var resEsperado = 187;

            var resFinal = game.Puntuacion();

            Assert.AreEqual(resFinal, resEsperado);
        }

        [Test]
        public void TestCaso05()//7 lanzamientos
        {

            game.NumeroPino(10);
            game.NumeroPino(9);     game.NumeroPino(1);
            game.NumeroPino(5);     game.NumeroPino(5);
            game.NumeroPino(7);     game.NumeroPino(2);
            game.NumeroPino(10);
            game.NumeroPino(9);     game.NumeroPino(0);
            game.NumeroPino(8);     game.NumeroPino(2);

            var resEsperado = 99;

            var resFinal = game.Puntuacion();

            Assert.AreEqual(resFinal, resEsperado);
        }


        [Test]
        public void TestCaso06()//todas las jugadas "0" pinos tumbados
        {

            game.MultiplesJugadas(20, 0);

            var resEsperado = 0;

            var resFinal = game.Puntuacion();

            Assert.AreEqual(resFinal, resEsperado);

        }



        [Test]
        public void TestCaso07()//10 lansamientos
        {

            game.NumeroPino(10);
            game.NumeroPino(10);
            game.NumeroPino(10);
            game.NumeroPino(10);
            game.NumeroPino(10);
            game.NumeroPino(10);
            game.NumeroPino(10);
            game.NumeroPino(10);
            game.NumeroPino(10);
            game.NumeroPino(10);
            game.NumeroPino(10);
            game.NumeroPino(10);


            var resEsperado = 300;

            var resFinal = game.Puntuacion();

            Assert.AreEqual(resFinal, resEsperado);
        }

        [Test]
        public void TestCaso08()//todas las jugadas "0" pinos tumbados
        {

            game.MultiplesJugadas(20, 0);

            var resEsperado = 0;

            var resFinal = game.Puntuacion();

            Assert.AreEqual(resFinal, resEsperado);

        }



        [Test]
        public void TestCaso09()//Juego Perfecto strike 12 jugadas
        {
            game.MultiplesJugadas(12, 10);

            var resEsperado = 300;

            var resFinal = game.Puntuacion();

            Assert.AreEqual(resFinal, resEsperado);

        }


        [Test]
        public void TestCaso10()//7 lanzamientos
        {

            game.NumeroPino(10);
            game.NumeroPino(9); game.NumeroPino(1);
            game.NumeroPino(5); game.NumeroPino(5);
            game.NumeroPino(7); game.NumeroPino(2);
            game.NumeroPino(10);
            game.NumeroPino(9); game.NumeroPino(0);
            game.NumeroPino(8); game.NumeroPino(2);

            var resEsperado = 99;

            var resFinal = game.Puntuacion();

            Assert.AreEqual(resFinal, resEsperado);
        }


        [Test]
        public void TestCaso11()//Juego Perfecto strike 12 jugadas
        {
            game.MultiplesJugadas(12, 10);

            var resEsperadoJugador1 = 300;

            var resFinalJuagador1 = game.Puntuacion();

            Assert.AreEqual(resFinalJuagador1, resEsperadoJugador1);



            game.MultiplesJugadas(12, 10);

            var resEsperadoJugador2 = 300;

            var resFinalJuagodor2 = game.Puntuacion();

            Assert.AreEqual(resEsperadoJugador2, resEsperadoJugador2);


        }


        [Test]
        public void TestCaso12()//Juego Perfecto strike 12 jugadas vs jugo 0
        {
            game.MultiplesJugadas(12, 10);

            var resEsperadoJugador1 = 300;

            var resFinalJuagador1 = game.Puntuacion();

            Assert.AreEqual(resFinalJuagador1, resEsperadoJugador1);



            game.MultiplesJugadas(20, 0);

            var resEsperadoJugador2 = 0;

            var resFinalJuagodor2 = game.Puntuacion();

            Assert.AreEqual(resEsperadoJugador2, resEsperadoJugador2);


        }


        [Test]
        public void TestCaso13()
        {
            game.MultiplesJugadas(20, 5);

            var resEsperadoJugador1 = 145;

            var resFinalJuagador1 = game.Puntuacion();

            Assert.AreEqual(resFinalJuagador1, resEsperadoJugador1);




            game.MultiplesJugadas(20, 0);

            var resEsperadoJugador2 = 0;

            var resFinalJuagodor2 = game.Puntuacion();

            Assert.AreEqual(resEsperadoJugador2, resEsperadoJugador2);


        }



        [Test]
        public void TestCaso14()
        {


            game.MultiplesJugadas(12, 10);

            var resEsperadoJugador = 300;

            var resFinalJuagador = game.Puntuacion();

            Assert.AreEqual(resFinalJuagador, resEsperadoJugador);





            game.MultiplesJugadas(12, 10);

            var resEsperadoJugador1 = 300;

            var resFinalJuagador1 = game.Puntuacion();

            Assert.AreEqual(resFinalJuagador1, resEsperadoJugador1);




            game.MultiplesJugadas(20, 0);

            var resEsperadoJugador2 = 0;

            var resFinalJuagodor2 = game.Puntuacion();

            Assert.AreEqual(resEsperadoJugador2, resEsperadoJugador2);


        }


        [Test]
        public void TestCaso15()//7 frames (turnos) 12 lanzamientos
        {

            game.NumeroPino(10);
            game.NumeroPino(9); game.NumeroPino(1);
            game.NumeroPino(5); game.NumeroPino(5);
            game.NumeroPino(7); game.NumeroPino(2);
            game.NumeroPino(10);
            game.NumeroPino(9); game.NumeroPino(0);
            game.NumeroPino(8); game.NumeroPino(2);

            var resEsperado = 99;

            var resFinal = game.Puntuacion();

            Assert.AreEqual(resFinal, resEsperado);



            game.MultiplesJugadas(12, 0);

            var resEsperadoJugador1 = 99;

            var resFinalJuagador1 = game.Puntuacion();

            Assert.AreEqual(resFinalJuagador1, resEsperadoJugador1);

        }


    }

}
