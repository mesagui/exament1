﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenT1
{
    public class Game
    {
        
        int[] numerLanzamientos = new int[100];
        int numJugadas;

        public void NumeroPino(int numPino)
        {
            numerLanzamientos[numJugadas] = numPino;
            numJugadas++;
        }


        public void MultiplesJugadas(int lansamientos, int numPino)
        {
            var a = numPino;

            for (int i = 0; i < lansamientos; i++)
            {
                
                NumeroPino(a);

            }

        }


        public int Puntuacion()
        {
            int score = 0, i = 0;

            for (int frame = 0; frame < 10; frame++)
            {
                if(numerLanzamientos[i] == 10)
                {
                    score += 10 + numerLanzamientos[i + 1] + numerLanzamientos[i + 2];
                    i += 1;
                }
                else if (numerLanzamientos[i] + numerLanzamientos[i + 1] == 10)
                {
                    score += 10 + numerLanzamientos[i + 2];
                    i += 2;
                }
                else
                {
                    score += numerLanzamientos[i] + numerLanzamientos[i + 1];
                    i += 2;
                }

            }


            return score;
        }

    }

}
